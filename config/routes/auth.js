const router = require("express").Router();
const isSuperAdmin = require("../../midleware/isSuperAdmin");
const auth = require("../../midleware/auth");
const { login, register, toAdmin } = require("../../app/controllers/authController");

router.post("/login", login);
router.post("/register", register);
router.put("/makeAdmin/:id", auth, isSuperAdmin, toAdmin);

module.exports = router;
