const router = require("express").Router();
const Auth = require("./auth");
const cars = require("./cars");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../../docs/swagger.json");

// API server
router.use("/api/auth/", Auth);
router.use("/api/cars/", cars);

router.use("/api-docs", swaggerUi.serve);
router.get("/api-docs", swaggerUi.setup(swaggerDocument));

module.exports = router;
