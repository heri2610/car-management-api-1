const auth = require("../services/authService");

const login = async (req, res) => {
  try {
    const token = await auth.login(req.body);
    res.status(200).json({
      message: "Berhasil login",
      token,
    });
  } catch (error) {
    res.status(error.statusCode || 500).json({
      message: error.message,
    });
  }
};
const register = async (req, res) => {
  try {
    const newUser = await auth.register(req.body);
    res.status(200).json({
      message: "registrasi success",
      newUser,
    });
  } catch (error) {
    res.status(error.statusCode || 500).json({
      message: error.message,
    });
  }
};
const toAdmin = async (req, res) => {
  try {
    const newAdmin = await auth.toAdmin(req.params.id);
    res.status(201).json({
      message: "berhasil dijadikan admin",
    });
  } catch (error) {
    res.status(error.statusCode || 500).json({
      message: error.message,
    });
  }
};
module.exports = { login, register, toAdmin };
