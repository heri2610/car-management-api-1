const repository = require("../repositories/carsRepository");
const { gatUser } = require("../repositories/userRepository");
const { create, updateBy, deleteBy } = require("../repositories/historyRepository");

const ApiError = require("../../utils/ApiError");
const getCars = async () => {
  const cars = await repository.getCars();
  if (!cars) throw new ApiError();
  return cars;
};
const getCarsWithMember = async () => {
  const cars = await repository.getCarsWithMember();
  if (!cars) throw new ApiError();
  const carsAvailable = cars.filter((car) => car.isActive == true);
  return carsAvailable;
};
const createCars = async (reqBody, userId) => {
  // cari nama pembuatnya berdasarkan id yg dikirm dari autentiikasi
  const userAktif = await gatUser(userId);
  // memasukan nama pembuat data mobil ke tabel history
  const Createhistory = await create(userAktif.name);
  // mendapatkan  id dari tabel history yg baru dibuat untuk dimasukan ke fild historyId
  const historyId = Createhistory.id;
  // mendapatkan data dari inputan user untuk membuat data cars baru
  const { name, price, size } = reqBody;
  const reqbody = { name, price, size, isActive: true, historyId };
  return await repository.createCars(reqbody);
};
const updateCars = async (carsId, reqBody, userId) => {
  const userAktif = await gatUser(userId);
  const { name, price, size } = reqBody;
  const reqbody = { name, price, size };
  const CarById = await repository.getCarById(carsId);
  await updateBy(userAktif.name, CarById.historyId);
  await repository.updateCars(carsId, reqbody);
  return await repository.getCarById(carsId);
};
const deleteCars = async (userId, carsId) => {
  const userAktif = await gatUser(userId);
  const CarById = await repository.getCarById(carsId);
  await deleteBy(userAktif.name, CarById.historyId);
  return await repository.deleteCars(carsId);
};

module.exports = { getCars, getCarsWithMember, createCars, updateCars, deleteCars };
